# Generated by Django 3.1.2 on 2022-04-17 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0004_auto_20220417_1711'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('price', models.IntegerField(default=0)),
                ('unit', models.CharField(max_length=128)),
                ('note', models.CharField(max_length=128)),
            ],
            options={
                'db_table': 'product',
                'ordering': ['name', 'price'],
            },
        ),
    ]

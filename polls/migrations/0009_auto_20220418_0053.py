# Generated by Django 3.1.2 on 2022-04-17 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0008_auto_20220418_0052'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='place',
            index=models.Index(fields=['name'], name='polls_place_name_9abf22_idx'),
        ),
    ]

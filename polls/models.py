from django.db import models
from sqlalchemy import true
from django.contrib.postgres.indexes import HashIndex
 
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self) -> str:
        return "questtion: {}".format(self.question_text)
 

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return "{} --- {}".format(self.question, self.choice_text)


class Account(models.Model):
    username = models.CharField(max_length=16)
    first_name = models.CharField(max_length=16)
    last_name = models.CharField(max_length=16)
    age = models.IntegerField(default=0)
    email = models.EmailField(primary_key=True)
    address = models.CharField(max_length=128, null=True, blank=True)

    def get_fullname(self):
        return '{} {}'.format(self.first_name, self.last_name)
    
    def info(self):
        return "{} {} is {} years old. The email of user {} is {}.\n {}'s address is {}".format(
            self.first_name,
            self.last_name,
            self.age,
            self.username,
            self.email,
            self.get_fullname(), 
            self.address  
        )

class BasePerson():
    def get_fullname(self):
        return "{} {}".format(self.first_name, self.last_name)

class Musician(models.Model, BasePerson):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    instrument = models.CharField(max_length=100)
    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
 
class Album(models.Model):
    artist = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars = models.IntegerField()
    def __str__(self):
        return self.name


 
class Person(models.Model, BasePerson):
    username = models.CharField(max_length=128)
    email = models.EmailField(unique=true)
    def __str__(self):
        return self.username
 
class Group(models.Model):
    name = models.CharField(max_length=128)
    members = models.ManyToManyField(Person, through='Membership')
    def __str__(self):
        return self.name

 
class Membership(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    date_joined = models.DateField()
    invite_reason = models.CharField(max_length=64)
    def __str__(self):
        return "{}: {}".format(self.person, self.group)

class Product(models.Model):
    name = models.CharField(max_length=128)
    price = models.IntegerField(default=0)
    unit = models.CharField(max_length=128)
    note =  models.CharField(max_length=128)

    class Meta:
        ordering = ['name', 'price']
        db_table = 'product'


class Place(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=80)
    class Meta:
        indexes = [
            models.Index(fields=['name']),
        ]
 
class Restaurant(Place):
    serves_hot_dogs = models.BooleanField(default=False)
    serves_pizza = models.BooleanField(default=False)
    class Meta:
        indexes = [
            # models.Index(fields=['name']),
            HashIndex(fields=['serves_hot_dogs']),
        ]

# -----------------------------------------------
class Blog(models.Model):
    name = models.CharField(max_length=100)
    tagline = models.TextField()
 
    def __str__(self): 
        return self.name
 
class Author(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
 
    def __str__(self): 
        return self.name

class Entry(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    headline = models.CharField(max_length=255)
    body_text = models.TextField()
    pub_date = models.DateField()
    mod_date = models.DateField()
    authors = models.ManyToManyField(Author)
    n_comments = models.IntegerField()
    n_pingbacks = models.IntegerField()
    rating = models.IntegerField()
 
    def __str__(self): 
        return self.headline
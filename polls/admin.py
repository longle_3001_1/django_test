from tokenize import group
from django.contrib import admin
from .models import Question, Choice, Account, Musician, Album, Person, Group, Membership

# Register your models here.
class ChoiceInLine(admin.StackedInline):
    model = Choice 
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date']}),
    ]
    inlines = [ChoiceInLine]


class AlbumInLine(admin.StackedInline):
    model = Album 
    extra = 0


class MusiciannAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Name',                   {'fields': ['first_name', 'last_name']}),
        ('Instrument information', {'fields': ['instrument']}),
    ]
    inlines = [AlbumInLine]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)
admin.site.register(Account)
admin.site.register(Musician, MusiciannAdmin)
admin.site.register(Album)
admin.site.register(Person)
admin.site.register(Membership)
admin.site.register(Group)

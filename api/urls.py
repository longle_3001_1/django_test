from django.urls import path

from . import views

urlpatterns = [
    path('post', views.ListCreateCarView.as_view()),
    path('post/<int:pk>', views.UpdateDeleteCarView.as_view()),
]
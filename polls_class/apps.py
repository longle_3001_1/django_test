from django.apps import AppConfig


class PollsClassConfig(AppConfig):
    name = 'polls_class'
